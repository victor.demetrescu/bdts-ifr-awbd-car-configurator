package com.bdts.ifr.awbd.carconfigurator.entity.catalog;

import com.bdts.ifr.awbd.carconfigurator.entity.generic.GenericEntity;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "COUNTRIES")
@Data
@EqualsAndHashCode(callSuper = true)
public class Country extends GenericEntity {

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "country")
    private Set<Manufacturer> manufacturers = new HashSet<>();
}
