package com.bdts.ifr.awbd.carconfigurator.entity.generic;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@MappedSuperclass
public abstract class GenericEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID", unique = true, updatable = false, nullable = false)
    @NotNull
    private Long id;

    @Column(name = "NAME", unique = true, updatable = false, nullable = false)
    @NotEmpty
    private String name;

}
