package com.bdts.ifr.awbd.carconfigurator.entity.car.components;

import com.bdts.ifr.awbd.carconfigurator.entity.generic.GenericEntity;
import com.bdts.ifr.awbd.carconfigurator.entity.generic.GenericPropertyValue;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;


@Entity
@Table(name = "ENGINES")
@Data
@EqualsAndHashCode(callSuper = true)
public class Engine extends GenericEntity {

    @Embedded
    @AttributeOverrides({
            @AttributeOverride( name = "key", column = @Column(name = "MAX_POWER", updatable = false, nullable = false)),
            @AttributeOverride( name = "value", column = @Column(name = "MAX_POWER_RPM", updatable = false, nullable = false))
    })
    private GenericPropertyValue<Double, Integer> maxPowerAtRPM;

    @Embedded
    @AttributeOverrides({
            @AttributeOverride( name = "key", column = @Column(name = "MAX_TORQUE", updatable = false, nullable = false)),
            @AttributeOverride( name = "value", column = @Column(name = "MAX_TORQUE_RPM", updatable = false, nullable = false))
    })
    private GenericPropertyValue<Double, Integer> maxTorqueAtRPM;

    private Integer numberOfCylinders;
    private Integer maxPowerOutputInKW;
    private Integer maxTorqueOutputInNM;
}
