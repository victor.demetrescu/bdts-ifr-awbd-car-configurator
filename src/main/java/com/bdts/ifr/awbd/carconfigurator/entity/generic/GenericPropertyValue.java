package com.bdts.ifr.awbd.carconfigurator.entity.generic;

import lombok.Data;

import javax.persistence.Embeddable;

@Data
@Embeddable
public class GenericPropertyValue<K, V> {

    private K key;
    private V value;
}
