package com.bdts.ifr.awbd.carconfigurator.entity.car;

import com.bdts.ifr.awbd.carconfigurator.entity.generic.GenericEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "CAR_MODEL_VERSION_VARIANTS")
@Data
@EqualsAndHashCode(callSuper = true)
public class CarModelVersionVariant extends GenericEntity {


}
