package com.bdts.ifr.awbd.carconfigurator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarConfiguratorService {

	public static void main(String[] args) {
		SpringApplication.run(CarConfiguratorService.class, args);
	}

}
