package com.bdts.ifr.awbd.carconfigurator.entity.car;

import com.bdts.ifr.awbd.carconfigurator.entity.catalog.Manufacturer;
import com.bdts.ifr.awbd.carconfigurator.entity.generic.GenericEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "CAR_MODELS")
@Data
@EqualsAndHashCode(callSuper = true)
public class CarModel extends GenericEntity {

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "MANUFACTURER_ID", nullable = false)
    @Fetch(value = FetchMode.JOIN)
    @NotNull
    private Manufacturer manufacturer;


    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "model", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CarModelVersion> versions = new HashSet<>();
}
