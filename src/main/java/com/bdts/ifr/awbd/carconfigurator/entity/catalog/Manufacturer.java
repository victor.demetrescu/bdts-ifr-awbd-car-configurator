package com.bdts.ifr.awbd.carconfigurator.entity.catalog;

import com.bdts.ifr.awbd.carconfigurator.entity.car.CarModel;
import com.bdts.ifr.awbd.carconfigurator.entity.generic.GenericEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "MANUFACTURERS")
@Data
@EqualsAndHashCode(callSuper = true)
public class Manufacturer extends GenericEntity {

    @Column(name = "DESCRIPTION", nullable = false)
    @NotEmpty
    private String description;

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "COUNTRY_ID", nullable = false)
    @Fetch(value = FetchMode.JOIN)
    @NotNull
    private Country country;

    @JsonManagedReference
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "manufacturer", cascade = CascadeType.ALL, orphanRemoval = true)
    private Set<CarModel> models = new HashSet<>();

}
