package com.bdts.ifr.awbd.carconfigurator.entity.car;

import com.bdts.ifr.awbd.carconfigurator.entity.generic.GenericEntity;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
@Table(name = "CAR_MODEL_VERSIONS")
@Data
@EqualsAndHashCode(callSuper = true)
public class CarModelVersion extends GenericEntity {

    @JsonBackReference
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "CAR_MODEL_ID", nullable = false)
    @Fetch(value = FetchMode.JOIN)
    @NotNull
    private CarModel model;

    @Column(name = "PRODUCTION_START", nullable = false)
    @NotNull
    private LocalDate productionStart;

    @Column(name = "PRODUCTION_END")
    private LocalDate productionEnd;
}
