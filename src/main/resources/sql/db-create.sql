create database car_configurator;
create user 'car'@'localhost' identified by 'car-config-password';
grant all privileges on car_configurator.* to 'car'@'localhost';
